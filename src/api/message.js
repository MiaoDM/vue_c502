import request from '@/utils/request'

export default {
  getSelectedUser(userStatus, fillStatus) {
    return request({
      url: `/message/getSelectedUser?userStatus=${userStatus}&fillStatus=${fillStatus}`,
      method: 'get'
    })
  },
  getTemplateList() {
    return request({
      url: `/message/getTemplateList`,
      method: 'get'
    })
  },
  getMessageContent(templateId) {
    return request({
      url: `/message/getMessageContent?templateId=${templateId}`,
      method: 'get'
    })
  },
  saveAsTemplate(templateTitle, templateContext) {
    return request({
      url: `/message/saveAsTemplate`,
      method: 'post',
      data: {
        templateTitle: templateTitle,
        templateContext: templateContext
      }
    })
  },
  sendMessage(selectedUserList, context) {
    return request({
      url: `/message/sendMessage`,
      method: 'post',
      data: {
        selectedUserList,
        context
      }
    })
  }
}

